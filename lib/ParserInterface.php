<?php

namespace rns\lib;

/**
 * Интерфейс-парсер.
 */
interface ParserInterface
{
    /**
     * Парсит строку в массив.
     *
     * @param string $data Разбираемые данные.
     *
     * @return array
     */
    public function parse($data);
}
