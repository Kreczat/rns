<?php

namespace rns\lib;

use PDO;

/**
 * Трейт, знающий об объекте БД PostgreSQL.
 */
trait DbAwareTrait
{
    /** @var PDO */
    protected $db;

    /**
     * Получает конфигурацию БД.
     *
     * @return array
     */
    protected function getDbConfig()
    {
        include_once __DIR__ . '/../config.php';

        return $config['db'];
    }

    /**
     * @inheritdoc
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @inheritdoc
     */
    public function setDb($db)
    {
        $this->db = $db;

        return $this;
    }
}