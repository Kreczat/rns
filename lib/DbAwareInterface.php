<?php

namespace rns\lib;

use PDO;

/**
 * Интерфейс, знающий о БД.
 */
interface DbAwareInterface
{
    /**
     * Получает объект БД.
     *
     * @return PDO
     */
    public function getDb();

    /**
     * @param PDO $db Объект БД.
     *
     * @return static
     */
    public function setDb($db);
}
