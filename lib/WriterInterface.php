<?php

namespace rns\lib;

/**
 * Интерфейс записи.
 */
interface WriterInterface
{
    /**
     * Записывает данные.
     *
     * @param array $data Данные.
     */
    public function write($data);
}
