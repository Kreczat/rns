<?php

namespace rns\lib;

use Exception;
use PDO;

/**
 * Класс менеджера управления шинами.
 */
class TireManager implements DbAwareInterface, ReaderInterface, ParserInterface, WriterInterface
{
    use DbAwareTrait;

    public function __construct()
    {
        $dbConfig = $this->getDbConfig();

        $db = new PDO(
            sprintf(
                'pgsql:dbname=%s;host=%s;port=%s',
                $dbConfig['name'],
                $dbConfig['host'],
                $dbConfig['port']
            ),
            $dbConfig['user'],
            $dbConfig['pass']
        );

        $this->setDb($db);
    }

    /**
     * @inheritdoc
     */
    public function read()
    {
        return $this->db->query(
            'SELECT * FROM in_data WHERE success IS NULL ORDER BY id ASC',
            PDO::FETCH_ASSOC
        )->fetchAll();
    }

    /**
     * Обрабатывает считанные данные.
     *
     * @param array $data
     */
    public function process($data)
    {
        foreach ($data as $row) {
            $success = true;
            try {
                $this->parse($row['tire']);
            } catch (Exception $e) {
                $success = false;
            } finally {
                $this->db->query(
                    sprintf(
                        'UPDATE in_data SET success = %s WHERE id = %d',
                        var_export($success, true),
                        $row['id']
                    )
                );
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function parse($data)
    {
        $tokens = explode(' ', $data);
        $tokenCount = count($tokens);
        $tire = ['brand' => $tokens[0]];
        $i = 1;
        for (; $i < $tokenCount; $i++) {
            if (preg_match('/\d+\/\d+/', $tokens[$i])) {
                break;
            }
        }
        $tire['model'] = implode(' ', array_slice($tokens, 1, $i-1));

        // Сохраним бренд и модель в таблицу шин и узнаем идентификатор сохранённой записи.
        $res = $this->db->query(
            sprintf(
                "INSERT INTO tire (brand, model) VALUES (%s, %s) 
                ON CONFLICT (brand, model) DO UPDATE 
                SET brand = EXCLUDED.brand 
                RETURNING id",
                $this->db->quote($tire['brand']),
                $this->db->quote($tire['model'])
            ),
            PDO::FETCH_NUM
        )->fetch();

        $properties = ['tire_id' => $res[0]];
        $subtokens = explode('/', $tokens[$i]);
        $properties['width'] = $subtokens[0];

        $subtokens = preg_split('/([a-z]+)/i', $subtokens[1], 3, PREG_SPLIT_DELIM_CAPTURE);
        $properties['height'] = $subtokens[0];
        $properties['design'] = $subtokens[1];
        $properties['diameter'] = $subtokens[2];

        $subtokens = preg_split('/([a-z]+\d*)/i', $tokens[$i+1], 2, PREG_SPLIT_DELIM_CAPTURE);
        $properties['load'] = $subtokens[0];
        $properties['speed'] = $subtokens[1];

        for ($i = $i + 2; $i < $tokenCount; $i++) {
            if ($this->checkSeason($tokens[$i])) {
                $properties['season'] = $tokens[$i];
                if (isset($tokens[$i + 1])) {
                    $properties['season'] .= ' ' . $tokens[$i + 1];
                    $i++;
                }
                continue;
            }

            if ($this->checkChamberiness($tokens[$i])) {
                $properties['chamberiness'] = $tokens[$i];
                continue;
            }

            if (!isset($tokens[$i + 1])) {
                throw new Exception('error');
            }

            if ($this->checkRunFlat($tokens[$i], $tokens[$i + 1])) {
                $properties['runflat'] = $tokens[$i];
                if ('Run' === $tokens[$i]) {
                    $properties['runflat'] .= ' ' . $tokens[$i + 1];
                    $i++;
                }
                continue;
            }

            if (isset($properties['abbreviation'])) {
                throw new Exception('error');
            }
            $properties['abbreviation'] = $tokens[$i];
        }

        $newData = preg_replace('/\s{2,}/', ' ', sprintf(
            '%s %s %d/%d%s%d %d%s %s %s %s %s',
            $tire['brand'],
            $tire['model'],
            $properties['width'],
            $properties['height'],
            $properties['design'],
            $properties['diameter'],
            $properties['load'],
            $properties['speed'],
            isset($properties['abbreviation']) ? $properties['abbreviation'] : '',
            isset($properties['runflat']) ? $properties['runflat'] : '',
            isset($properties['chamberiness']) ? $properties['chamberiness'] : '',
            $properties['season']
        ));

        if ($data !== $newData) {
            throw new Exception('error');
        }

        $this->write($properties);
    }

    /**
     * Проверяет значение характеристики "сезон".
     *
     * @param string $value Проверяемое значение.
     *
     * @return bool
     */
    protected function checkSeason($value)
    {
        switch ($value) {
            case 'Летние':
            case 'Зимние':
            case 'Всесезонные':
            case 'Внедорожные':
                return true;
            default:
                return false;
        }
    }

    /**
     * Проверяет значение характеристики "камерность".
     *
     * @param string $value Проверяемое значение.
     *
     * @return bool
     */
    protected function checkChamberiness($value)
    {
        switch ($value) {
            case 'TT':
            case 'TL':
            case 'TL/TT':
                return true;
            default:
                return false;
        }
    }

    /**
     * Проверяет значение характеристики "Run Flat".
     *
     * @param string $value     Проверяемое значение.
     * @param string $nextValue Следующее значение.
     *
     * @return bool
     */
    protected function checkRunFlat($value, $nextValue)
    {
        switch ($value) {
            case 'RunFlat':
            case 'ROF':
            case 'ZP':
            case 'SSR':
            case 'ZPS':
            case 'HRS':
            case 'RFT':
                return true;
            case 'Run':
                return ('Flat' === $nextValue);
            default:
                return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function write($data)
    {
        $query = sprintf(
            "INSERT INTO property 
            (tire_id, width, height, design, diameter, load, speed, abbreviaton, runflat, chamberiness, season) 
            VALUES (%d, %d, %d, %s, %d, %d, %s, %s, %s, %s, %s)",
            $data['tire_id'],
            $data['width'],
            $data['height'],
            $this->db->quote($data['design']),
            $data['diameter'],
            $data['load'],
            $this->db->quote($data['speed']),
            isset($data['abbreviation']) ? $this->db->quote($data['abbreviation']) : 'null',
            isset($data['runflat']) ? $this->db->quote($data['runflat']) : 'null',
            isset($data['chamberiness']) ? $this->db->quote($data['chamberiness']) : 'null',
            $this->db->quote($data['season'])
        );

        $this->db->query($query);
    }
}
