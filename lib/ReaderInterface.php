<?php

namespace rns\lib;

/**
 * Интерфейс чтения.
 */
interface ReaderInterface
{
    /**
     * Читает данные.
     *
     * @return array
     */
    public function read();
}
