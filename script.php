<?php

use rns\lib\TireManager;

spl_autoload_register(function($className) {
    require_once __DIR__ . '/../' . str_replace('\\', '/', $className) . '.php';
});

$tireManager = new TireManager();

$data = $tireManager->read();
$tireManager->process($data);